/* ***** BEGIN LICENSE BLOCK *****
 * Version: MIT/X11 License
 * 
 * Copyright (c) 2010 Erik Vold
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * Contributor(s):
 *   Erik Vold <erikvvold@gmail.com> (Original Author)
 *   Greg Parris <greg.parris@gmail.com>
 *   Nils Maier <maierman@web.de>
 *   Szabolcs Hubai <szab.hu@gmail.com>
 *
 * ***** END LICENSE BLOCK ***** */

const {classes: Cc, interfaces: Ci, utils: Cu} = Components;
Cu.import("resource://gre/modules/Services.jsm");

const NS_XUL = "http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul";
const keysetID = "wdttoggler-keyset";
const keyID = "RR:WDTToggler";
const fileMenuitemID = "menu_WDTToggleritem";
var XUL_APP = {name: Services.appinfo.name};

switch(Services.appinfo.name) {
case "Thunderbird":
  XUL_APP.winType = "mail:3pane";
  XUL_APP.wdttogglerKeyset = "mailKeys";
  break;
case "Fennec": break;
default: //"Firefox", "SeaMonkey"
  XUL_APP.winType = "navigator:browser";
  XUL_APP.wdttogglerKeyset = "mainKeyset";
}

const PREF_BRANCH = "extensions.wdttoggler.";
const PREFS = {
	currentversion:"0",
	reinstall:false,
	sbiconhide:false,
	customwdttogglerkeyboardshortcut:""
};

var prefChgHandlers = [];
let PREF_OBSERVER = {
  observe: function(aSubject, aTopic, aData) {
    if ("nsPref:changed" != aTopic || !(aData in PREFS)) return;
    prefChgHandlers.forEach(function(func) func && func(aData));
  }
}

let logo = "";

/* Includes a javascript file with loadSubScript
*
* @param src (String)
* The url of a javascript file to include.
*/
(function(global) global.include = function include(src) {
  var o = {};
  Components.utils.import("resource://gre/modules/Services.jsm", o);
  var uri = o.Services.io.newURI(
      src, null, o.Services.io.newURI(__SCRIPT_URI_SPEC__, null, null));
  o.Services.scriptloader.loadSubScript(uri.spec, global);
})(this);

/* Imports a commonjs style javascript file with loadSubScrpt
 * 
 * @param src (String)
 * The url of a javascript file.
 */
(function(global) {
  var modules = {};
  global.require = function require(src) {
    if (modules[src]) return modules[src];
    var scope = {require: global.require, exports: {}};
    var tools = {};
    Components.utils.import("resource://gre/modules/Services.jsm", tools);
    var wdttogglerURI = tools.Services.io.newURI(__SCRIPT_URI_SPEC__, null, null);
    try {
      var uri = tools.Services.io.newURI(
          "packages/" + src + ".js", null, wdttogglerURI);
      tools.Services.scriptloader.loadSubScript(uri.spec, scope);
    } catch (e) {
      var uri = tools.Services.io.newURI(src, null, wdttogglerURI);
      tools.Services.scriptloader.loadSubScript(uri.spec, scope);
    }
    return modules[src] = scope.exports;
  }
})(this);


var {unload} = require("unload");
var {runOnLoad, runOnWindows, watchWindows} = require("window-utils");
include("includes/l10n.js");
include("includes/prefs.js");


function setPref(aKey, aVal) {
  aVal = ("wrapper-wdttoggler-toolbarbutton" == aVal) ? "" : aVal;
  switch (typeof(aVal)) {
    case "string":
      var ss = Cc["@mozilla.org/supports-string;1"]
          .createInstance(Ci.nsISupportsString);
      ss.data = aVal;
      Services.prefs.getBranch(PREF_BRANCH)
          .setComplexValue(aKey, Ci.nsISupportsString, ss);
      break;
  }
}

function addMenuItem(win) {
  var $ = function(id) win.document.getElementById(id);

  function removeMI() {
    var menuitem = $(fileMenuitemID);
    menuitem && menuitem.parentNode.removeChild(menuitem);
  }
  removeMI();

  // add the new menuitem to File menu
  let (wdttogglerMI = win.document.createElementNS(NS_XUL, "menuitem")) {
    wdttogglerMI.setAttribute("id", fileMenuitemID);
    wdttogglerMI.setAttribute("class", "menuitem-iconic");
    wdttogglerMI.setAttribute("label", _("restart", getPref("locale")));
    wdttogglerMI.setAttribute("accesskey", "R");
    wdttogglerMI.setAttribute("key", keyID);
    wdttogglerMI.style.listStyleImage = "url('" + logo + "')";
    wdttogglerMI.addEventListener("command", restart, true);

    $("menu_FilePopup").insertBefore(wdttogglerMI, $("menu_FileQuitItem"));
  }

  unload(removeMI, win);
}

function restart() {
  let canceled = Cc["@mozilla.org/supports-PRBool;1"]
      .createInstance(Ci.nsISupportsPRBool);

  Services.obs.notifyObservers(canceled, "quit-application-requested", "restart");

  if (canceled.data) return false; // somebody canceled our quit request

  // disable fastload cache?
  if (getPref("disable_fastload")) Services.appinfo.invalidateCachesOnRestart();

  // restart
  Cc['@mozilla.org/toolkit/app-startup;1'].getService(Ci.nsIAppStartup)
      .quit(Ci.nsIAppStartup.eAttemptQuit | Ci.nsIAppStartup.eRestart);

  return true;
}

/***********************************************************************************************/






	function fileRead (nsiFile,char) {
		try {
			var sIS = Components.classes['@mozilla.org/scriptableinputstream;1' ].createInstance(Components.interfaces.nsIScriptableInputStream );
			var nIS = Components.classes[ '@mozilla.org/network/file-input-stream;1'].createInstance( Components.interfaces.nsIFileInputStream);
			var rdata = new String();
			nIS.init(nsiFile , 1 , 0, false);
			sIS.init(nIS);
			rdata += sIS.read(-1 );
			sIS.close();
			nIS.close();
			if (char) {
				try {
					var sUC = Components.classes[ '@mozilla.org/intl/scriptableunicodeconverter' ].createInstance( Components.interfaces.nsIScriptableUnicodeConverter);
					sUC.charset = char;
					rdata = sUC.ConvertToUnicode(rdata); 	
				} 
				catch(e) {}
			}
			return rdata;
		} 
		catch(e) {
			var prompt = Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService(Components.interfaces.nsIPromptService);					prompt.alert(null, "Toggle Web Developer Toolbar",e); 	
		}
	}
	

function addAddonOverlayScript (win,doc,$,xul) {

    var ios = Components.classes["@mozilla.org/network/io-service;1"].getService(Components.interfaces.nsIIOService);
	var file=addon.getResourceURI("chrome/content/wdttoggler_bootstrap.js");
	var cs= new Cu.Sandbox(win,{ sandboxPrototype: win, wantXrays: false });
	let loader = Cc["@mozilla.org/moz/jssubscript-loader;1"].createInstance(Ci.mozIJSSubScriptLoader);
	loader.loadSubScript(file.spec, cs);
	
}

function addStatusbarButton (win,doc,$,xul) {
	
  let wdttogglerTBB = xul("statusbarpanel");
  wdttogglerTBB.setAttribute("id", "wdttoggler-statusbarpanel");
  wdttogglerTBB.setAttribute("style", "cursor:pointer;"); 
  //wdttogglerTBB.setAttribute("onclick", "WDTToggler.handleStatusClick(event);");
  let wdttogglerTBB2 = xul("image");
  logo = addon.getResourceURI("images/icon16.png").spec;
  wdttogglerTBB2.setAttribute("src", logo);
  wdttogglerTBB.appendChild(wdttogglerTBB2);
  
  //wdttogglerTBB.addEventListener("command", restart, true);
  //wdttogglerTBB.setAttribute("onclick", "gBrowser.selectedTab = gBrowser.addTab('http://google.com/');");
 /* wdttogglerTBB.addEventListener("click",function(event){
  
		win.WDTToggler.bringGoogle(event,win);
 
  }, true);*/
  

  
  
   //wdttogglerTBB.addEventListener("click",win.WDTToggler.nn, true);
    
  $("status-bar").appendChild(wdttogglerTBB);
  
  function removeStatusbarButton () {
    
  	$("wdttoggler-statusbarpanel").parentNode.removeChild($("wdttoggler-statusbarpanel"));

  }
  
   unload(removeStatusbarButton, win);

}


function handleMainWindowLoad (win,doc,$,xul) {
	//WDTToggler.mainWindowLoadHandler(null,win);
}







/***********************************************************************************************/

function main(win) {
  let doc = win.document;
  function $(id) doc.getElementById(id);
  function xul(type) doc.createElementNS(NS_XUL, type);	
  
  	try {
		//addStatusbarButton(win,doc,$,xul);
		//it is handled in overlay.js
		//handleMainWindowLoad(win,doc,$,xul);
		//we need make sure that the interface elements are loaded first then the overlay.js maybe I can add this to the overlay.js too so it may be more reliable to know when to call the logic.
		addAddonOverlayScript(win,doc,$,xul);
		win.WDTToggler.init();
	}
	catch(err) {
		var prompt = Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService(Components.interfaces.nsIPromptService);					prompt.alert(null, "Toggle Web Developer Toolbar",err); 	
	}
	
  unload(function() {
    //wdttogglerKeyset.parentNode.removeChild(wdttogglerKeyset);
    //appMenu && appMenu.removeChild(wdttogglerAMI);
    //wdttogglerTBB.parentNode.removeChild(wdttogglerTBB);
    //win.removeEventListener("aftercustomization", saveTBNodeInfo);
    //prefChgHandlers[prefChgHandlerIndex] = null;
    win.WDTToggler.mainWindowUnloadHandler();
  }, win);
}






function main2(win) {
  let doc = win.document;
  function $(id) doc.getElementById(id);
  function xul(type) doc.createElementNS(NS_XUL, type);	
  
  	try {
		addAddonOverlayScript(win,doc,$,xul);
		win.WDTToggler.init2();
	}
	catch(err) {
		var prompt = Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService(Components.interfaces.nsIPromptService);					prompt.alert(null, "Toggle Web Developer Toolbar",err); 	
	}

  unload(function() {
    //wdttogglerKeyset.parentNode.removeChild(wdttogglerKeyset);
    //appMenu && appMenu.removeChild(wdttogglerAMI);
    //wdttogglerTBB.parentNode.removeChild(wdttogglerTBB);
    //win.removeEventListener("aftercustomization", saveTBNodeInfo);
    //prefChgHandlers[prefChgHandlerIndex] = null;
    win.WDTToggler.mainWindowUnloadHandler();
  }, win);
}

var addon = {
  getResourceURI: function(filePath) ({
    spec: __SCRIPT_URI_SPEC__ + "/../" + filePath
  })
}

function addResourceProtocol(data) {

	var ios=Components.classes["@mozilla.org/network/io-service;1"].getService(Components.interfaces.nsIIOService);
	var rh=ios.getProtocolHandler("resource").QueryInterface(Components.interfaces.nsIResProtocolHandler);
	var nfu=ios.newFileURI(data.installPath);
	var isDir=data.installPath.isDirectory();
	if (isDir) { nfu=ios.newURI(nfu.spec+"/resource/",null,null);} 
	else {nfu=ios.newURI("jar:"+nfu.spec+"!/resource/",null,null);}
	rh.setSubstitution("wdttoggler",nfu);

}

function removeResourceProtocol(data) {
	var ios=Components.classes["@mozilla.org/network/io-service;1"].getService(Components.interfaces.nsIIOService);
	var r=ios.getProtocolHandler("resource").QueryInterface(Components.interfaces.nsIResProtocolHandler);
	r.setSubstitution("wdttoggler",null);
}

function addChromeProtocol(data) {
	Components.manager.addBootstrappedManifestLocation(data.installPath);
}

function removeChromeProtocol(data) {
	Components.manager.removeBootstrappedManifestLocation(data.installPath);
}

function disable(id) {
  Cu.import("resource://gre/modules/AddonManager.jsm");
  AddonManager.getAddonByID(id, function(addon) {
    addon.userDisabled = true;
  });
}

function install(data) {
  if ("Fennec" == XUL_APP.name) disable(data.id);
}

function uninstall(data,reason){

	const APP_STARTUP 	= 1 
	const APP_SHUTDOWN  = 2 	
	const ADDON_ENABLE  = 3 
	const ADDON_DISABLE = 4
	const ADDON_INSTALL = 5
	const ADDON_UNINSTALL = 6
	const ADDON_UPGRADE   = 7
	const ADDON_DOWNGRADE = 8
		
	switch (reason) {
	  case ADDON_INSTALL:
	    break;
	  case ADDON_UNINSTALL:
		deletePref();			  
		break;
	  case ADDON_ENABLE:
		break;
	  case ADDON_DISABLE:
		break;
	  case ADDON_UPGRADE:
		break; 
	  case ADDON_DOWNGRADE:
		break;
	  case APP_STARTUP:
		break;
	  case APP_SHUTDOWN:
		break;
	  default:
	 }  

}




function addComponent() {

		Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");
		
		function AboutSitename() { }
		AboutSitename.prototype = {
		classDescription: "about:wdtt",
		contractID: "@mozilla.org/network/protocol/about;1?what=wdtt",
		classID: Components.ID("{6b4ba26a-922b-4a2c-b930-6b28c4848cc5}"),
		//Note: classID here should be exactly the same as CID in chrome.manifest
		QueryInterface: XPCOMUtils.generateQI([Ci.nsIAboutModule]),
		
		getURIFlags: function(aURI) {
		return Ci.nsIAboutModule.ALLOW_SCRIPT;
		},
		
		newChannel: function(aURI) {
		let ios = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);
		var file=addon.getResourceURI("chrome/content/options.xul");
		let channel = ios.newChannel(file.spec, null, null);
		var systemPrincipal = Components.classes["@mozilla.org/systemprincipal;1"].createInstance(Components.interfaces.nsIPrincipal);
		channel.originalURI = aURI;
			channel.owner=systemPrincipal;
		return channel;
		}
		};
		
		if (XPCOMUtils.generateNSGetFactory)
		    var NSGetFactory = XPCOMUtils.generateNSGetFactory([AboutSitename]);
		
		(function(){
		    var reg = Components.manager.QueryInterface(Components.interfaces.nsIComponentRegistrar)
		    var CONTRACT_ID = AboutSitename.prototype.contractID
		    try{
		            reg.unregisterFactory(
		                    reg.contractIDToCID(CONTRACT_ID),
		                    reg.getClassObjectByContractID(CONTRACT_ID, Ci.nsISupports)
		            )
		    }catch(e){}
		    var f1 = AboutSitename.prototype
		    var f = NSGetFactory(f1.classID)
		    reg.registerFactory(f1.classID, f1.classDescription, f1.contractID, f);
		})();

}







function startup(data, reason) {
  if ("Fennec" == XUL_APP.name) {
    if (ADDON_ENABLE == reason) restart();
    disable(data.id);
  }


  //addComponent();
  addResourceProtocol(data);
  addChromeProtocol(data);
  
  var prefs = Services.prefs.getBranch(PREF_BRANCH);

  // setup l10n
  //l10n(addon, "rr.properties");
  //unload(l10n.unload);

  // setup prefs
  setDefaultPrefs();

  //add prefs after settting the defaults
  addUserStyles();

  logo = addon.getResourceURI("images/icon16.png").spec;
				

				
			const APP_STARTUP 	= 1 
			const APP_SHUTDOWN  = 2 	
			const ADDON_ENABLE  = 3 
			const ADDON_DISABLE = 4
			const ADDON_INSTALL = 5
			const ADDON_UNINSTALL = 6
			const ADDON_UPGRADE   = 7
			const ADDON_DOWNGRADE = 8
				
			switch (reason) {
			  case ADDON_INSTALL:
			  watchWindows(main2, XUL_APP.winType);
			    break;
			  case ADDON_UNINSTALL:
				break;
			  case ADDON_ENABLE:
			  watchWindows(main2, XUL_APP.winType);
				break;
			  case ADDON_DISABLE:
				break;
			  case ADDON_UPGRADE:
			  watchWindows(main2, XUL_APP.winType);
				break; 
			  case ADDON_DOWNGRADE:
			  watchWindows(main2, XUL_APP.winType);
				break;
			  // The startup and shutdown strings are also used outside of
			  // lifeCycleObserver192.
			  case APP_STARTUP:
			  watchWindows(main2, XUL_APP.winType);
				break;
			  case APP_SHUTDOWN:
				break;
			  default:
			  watchWindows(main2, XUL_APP.winType);
			  	    
			 }   
  
   
  
  
  
  //prefs = prefs.QueryInterface(Components.interfaces.nsIPrefBranch2);
  //prefs.addObserver("", PREF_OBSERVER, false);
  //unload(function() prefs.removeObserver("", PREF_OBSERVER));
};

function addUserStyles(){
		var twdtprefsinstance = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefBranch);
		var twdtsbiconhide = twdtprefsinstance.getBoolPref("extensions.wdttoggler.sbiconhide");
		if (twdtsbiconhide) {
			var sss = Components.classes['@mozilla.org/content/style-sheet-service;1'].getService(Components.interfaces.nsIStyleSheetService);
			var ios = Components.classes['@mozilla.org/network/io-service;1'].getService(Components.interfaces.nsIIOService);
			var css = "@namespace url(http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul);#wdttoggler-toolbarbutton {display: none !important;}";
			var uri = ios.newURI("data:text/css," + encodeURIComponent(css), null, null);
			sss.loadAndRegisterSheet(uri, sss.USER_SHEET);
		}
}


function removeUserStyles(){
		var twdtprefsinstance = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefBranch);
		var twdtsbiconhide = twdtprefsinstance.getBoolPref("extensions.wdttoggler.sbiconhide");
		if (true) {
			var sss = Components.classes['@mozilla.org/content/style-sheet-service;1'].getService(Components.interfaces.nsIStyleSheetService);
			var ios = Components.classes['@mozilla.org/network/io-service;1'].getService(Components.interfaces.nsIIOService);
			var css = "@namespace url(http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul);#wdttoggler-toolbarbutton {display: none !important;}";
			var uri = ios.newURI("data:text/css," + encodeURIComponent(css), null, null);
			sss.unregisterSheet(uri, sss.USER_SHEET);
		}
}
		

function removeComponent(){
	    var reg = Components.manager.QueryInterface(Components.interfaces.nsIComponentRegistrar)
        var CONTRACT_ID = AboutSitename.prototype.contractID
                 
        try{
                reg.unregisterFactory(
                        reg.contractIDToCID(CONTRACT_ID),
                        reg.getClassObjectByContractID(CONTRACT_ID, Ci.nsISupports)
                )
        }catch(e){
        
  		var prompt = Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService(Components.interfaces.nsIPromptService);					prompt.alert(null, "Toggle Web Developer Toolbar",e); 	       
        
        }
}

function shutdown(data, reason) {

	unload();
	removeResourceProtocol();
  	removeChromeProtocol(data);	
	//removeComponent();
	removeUserStyles();
	
}







function log(e,toConsole) {

	if(!toConsole) {
  		var prompt = Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService(Components.interfaces.nsIPromptService);
  		prompt.alert(null, ADDON_LITERAL_NAME,e);
  	}
  	else{

		var aConsoleService = Components.classes["@mozilla.org/consoleservice;1"].getService(Components.interfaces.nsIConsoleService);
		aConsoleService.logStringMessage(e); 
		 	
  	}

}

function deletePref(){

	Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefService).getBranch("extensions.wdttoggler.").deleteBranch("");

}

