var websecurifyXmlhttprequestDispatcherClientHelpers = {
	domainWhitehistRegexp: null,
	
	isDocumentInScope: function (document) {
		return this.domainWhitehistRegexp != null && this.domainWhitehistRegexp.test(document.location);
	},
	
	getDocument: function () {
		return content.document;
	},
	
	createEvent: function (name) {
		return content.document.createEvent(name);
	},
	
	getElementById: function (id) {
		return content.document.getElementById(id);
	},
	
	getElementTextData: function ($element, data) {
		return $element.textContent;
	},
	
	setElementTextData: function ($element, data) {
		$element.textContent = data;
	}
};

/* ------------------------------------------------------------------------ */

(function () {
	function handleSetDomainWhitelist(event) {
		var regexpParts = [];
		
		for (var index in event.json.list) {
			var entry = event.json.list[index];
			
			var prefix;
			
			if (entry.sslonly) {
				prefix = 'https://';
			} else {
				prefix = 'https?://';
			}
			
			var domain = entry.domain;
			
			if (!domain) {
				return;
			}
			
			domain = domain.trim();
			
			if (!domain) {
				return;
			}
			
			regexpParts.push(prefix + domain.replace(/([.?*+^$[\]\\(){}|-])/g, '\\$1'));
		}
		
		websecurifyXmlhttprequestDispatcherClientHelpers.domainWhitehistRegexp = new RegExp('^(' + regexpParts.join('|') + ')(/|$)', 'i');
		
		removeMessageListener('websecurify-xmlhttprequest-set-domain-whitelist', arguments.callee);
	};
	
	addMessageListener('websecurify-xmlhttprequest-set-domain-whitelist', handleSetDomainWhitelist);
	
	/* -------------------------------------------------------------------- */
	
	sendAsyncMessage('websecurify-xmlhttprequest-get-domain-whitelist', {});
})();
