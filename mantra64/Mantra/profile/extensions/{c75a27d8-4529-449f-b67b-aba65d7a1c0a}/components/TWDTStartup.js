Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");

function TWDTStartup(){}
TWDTStartup.prototype={
   QueryInterface: function(aIID) {
    if(!aIID.equals(COIN.nsISupports) && !aIID.equals(COIN.nsIObserver)) throw CORE.NS_ERROR_NO_INTERFACE;
    return this;
  },
  classID: Components.ID("{4ddd4d9f-c6c3-4741-877e-8881f48639ff}"),
  classDescription: "TWDT Startup",  
  contractID: "@twdt/startup;1",
  xpcomobserversadded:false,  
  observe: function(aSubject,aTopic,aData) {
    switch(aTopic) {
      case "xpcom-startup":
       
        var observerService = COCL["@mozilla.org/observer-service;1"].getService(COIN.nsIObserverService);
        //observerService.addObserver(this,"quit-application",false);        
		observerService.addObserver(this,"profile-after-change",false);
		//observerService.addObserver(this,"quit-application-requested",false);
		this.xpcomobserversadded=true;
		
		break;
      case "profile-after-change":

        if(!this.xpcomobserversadded){
			var observerService = COCL["@mozilla.org/observer-service;1"].getService(COIN.nsIObserverService);
			observerService.addObserver(this,"quit-application",false);        
			//observerService.addObserver(this,"profile-after-change",false);
			observerService.addObserver(this,"quit-application-requested",false);	  
		}
		
		var twdtprefsinstance = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefBranch);
		var twdtsbiconhide = twdtprefsinstance.getBoolPref("extensions.wdttoggler.sbiconhide");
		if (twdtsbiconhide) {
			var sss = Components.classes['@mozilla.org/content/style-sheet-service;1'].getService(Components.interfaces.nsIStyleSheetService);
			var ios = Components.classes['@mozilla.org/network/io-service;1'].getService(Components.interfaces.nsIIOService);
			var css = "@namespace url(http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul);#wdttoggler-toolbarbutton {display: none !important;}";
			var uri = ios.newURI("data:text/css," + encodeURIComponent(css), null, null);
			sss.loadAndRegisterSheet(uri, sss.USER_SHEET);
		}	  
	  
       break;
      case "quit-application":
        
        break;
      case "quit-application-requested":

		break;		
      default:
        throw Components.Exception("Unknown topic: "+aTopic);
    }
  },
};
var objects=[TWDTStartup];
const COIN=Components.interfaces;  
const CORE=Components.results;
const COCL=Components.classes;
const TWDTOBSERVERNAME="TWDTStartup";
function FactHolder(aObjc){
  this.contractID=aObjc.prototype.contractID;
  this.className=aObjc.prototype.classDescription;
  this.CID=aObjc.prototype.classID;
  this.factory={
    createInstance:function(aOuter,aIID){
      if(aOuter) {throw CORE.NS_ERROR_NO_AGGREGATION;}
      return (new this.constructor).QueryInterface(aIID);
    }
  };
  this.factory.constructor=aObjc;
}
function NSGetModule(compMgr,flSpec){
  for(var i in objects) {bbModule._objects[i]=new FactHolder(objects[i]);}
  return bbModule;
}
var bbModule={
  _objects:{},
  canUnload:function(aComMan){
    return true;
  },
  getClassObject:function(aComMan,aCID,aIID) {
    if (!aIID.equals(COIN.nsIFactory)) throw CORE.NS_ERROR_NOT_IMPLEMENTED;
    for (var keyo in this._objects) {
      if (aCID.equals(this._objects[keyo].CID))
        return this._objects[keyo].factory;
    }
    throw CORE.NS_ERROR_NO_INTERFACE;
  },	  
  unregisterSelf:function(aCompMgr,aFlSpec,aLoc){
    var categoryManager=COCL["@mozilla.org/categorymanager;1"].getService(COIN.nsICategoryManager);
    categoryManager.deleteCategoryEntry("xpcom-startup",TWDTOBSERVERNAME,true);
    aComMan.QueryInterface(COIN.nsIComponentRegistrar);
    for (var keyo in this._objects) {
      var objc=this._objects[keyo];
      aComMan.unregisterFactoryLocation(objc.CID,aFlSpec);
    }
  },
  registerSelf:function(aComMan,aFlSpec,aLoc,aType){
    aComMan.QueryInterface(COIN.nsIComponentRegistrar);
    for (var keyo in this._objects){
      var objc=this._objects[keyo];
      aComMan.registerFactoryLocation(objc.CID,objc.className,objc.contractID,aFlSpec,aLoc,aType);
    }
    var categoryManager=COCL["@mozilla.org/categorymanager;1"].getService(COIN.nsICategoryManager);
    categoryManager.addCategoryEntry("xpcom-startup",TWDTOBSERVERNAME,TWDTStartup.prototype.contractID,true,true);
    categoryManager.addCategoryEntry("xpcom-shutdown", TWDTOBSERVERNAME,TWDTStartup.prototype.contractID,true,true);
  },	   
};

/**
* XPCOMUtils.generateNSGetFactory was introduced in Mozilla 2 (Firefox 4).
* XPCOMUtils.generateNSGetModule is for Mozilla 1.9.2 (Firefox 3.6).
*/
if (XPCOMUtils.generateNSGetFactory)
    var NSGetFactory = XPCOMUtils.generateNSGetFactory(objects);