var self = require("self");
var us = require("userstyles");

exports.main = function() {
  var url = self.data.url("firefox-better-addon-bar.css");
  us.load(url);
};