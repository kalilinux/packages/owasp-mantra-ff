const CR = Components.results;
const CC = Components.classes;
const CI = Components.interfaces;

/* ------------------------------------------------------------------------ */

Components.utils.import('resource://gre/modules/XPCOMUtils.jsm');

/* ------------------------------------------------------------------------ */

function WebsecurifyAboutModule() {
	// pass
}

WebsecurifyAboutModule.prototype = {
	classDescription: 'Websecurify About Module',
	classID: Components.ID('{9b45ca12-3982-4d8a-b243-a479f84e5072}'),
	contractID: '@mozilla.org/network/protocol/about;1?what=websecurify',
	QueryInterface: XPCOMUtils.generateQI([CI.nsIAboutModule]),
	
	/* -------------------------------------------------------------------- */
	
	get wrappedJSObject () {
		return this;
	},
	
	/* -------------------------------------------------------------------- */
	
	getURIFlags: function(uri) {
		return CI.nsIAboutModule.URI_SAFE_FOR_UNTRUSTED_CONTENT;
	},
	
	newChannel: function(uri) {
		let ioService = CC['@mozilla.org/network/io-service;1'].getService(CI.nsIIOService);
		let channel = ioService.newChannel('chrome://org.gnucitizen.websecurify.firefox/content/xul/aboutWindow.xul', null, null);
		
		channel.originalURI = uri;
		
		return channel;
	}
}

/* ------------------------------------------------------------------------ */

var NSGetFactory = XPCOMUtils.generateNSGetFactory([WebsecurifyAboutModule]);
