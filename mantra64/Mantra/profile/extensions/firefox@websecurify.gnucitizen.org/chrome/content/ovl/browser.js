function handleWebsecurifyOpenScreenCommandEvent(event) {
	switchToTabHavingURI('https://suite.websecurify.com', true);
}

/* ------------------------------------------------------------------------ */

function websecurifyAddToolbarbutton() {
	let $navBar = document.getElementById('nav-bar');
	let currentSetArray = $navBar.currentSet.split(',');
	
	if (currentSetArray.indexOf('websecurify-firefox-browser-websecurify-toolbarbutton') < 0) {
	 	currentSetArray.push('websecurify-firefox-browser-websecurify-toolbarbutton');
		
		let currentSet = currentSetArray.join(',');
		
		$navBar.setAttribute('currentset', currentSet);
		
		$navBar.currentSet = currentSet;
		
		document.persist('nav-bar', 'currentset');
	}
}

/* ------------------------------------------------------------------------ */

function handleWebsecurifyFirstrun() {
	websecurifyAddToolbarbutton();
}

function handleWebsecurifyUpgrade() {
	websecurifyAddToolbarbutton();
}

function handleWebsecurifyReinstall() {
	websecurifyAddToolbarbutton();
}

/* ------------------------------------------------------------------------ */

function handleWebsecurifyLoadEvent(event) {
	if (event.target != document) {
		return;
	}
	
	let firstrun;
	
	try {
		firstrun = Services.prefs.getBoolPref('extensions.org.gnucitizen.websecurify.firefox.firstrun');
	} catch (e) {
		firstrun = true;
	}
	
	Application.getExtensions(function(extensions) {
		let currentVersion = extensions.get('firefox@websecurify.gnucitizen.org').version;
		
		if (firstrun) {
			Services.prefs.setBoolPref('extensions.org.gnucitizen.websecurify.firefox.firstrun', false);
			Services.prefs.setCharPref('extensions.org.gnucitizen.websecurify.firefox.installedVersion', currentVersion);
			
			handleWebsecurifyFirstrun();
		} else {
			try {
				let installedVersion = Services.prefs.getCharPref('extensions.org.gnucitizen.websecurify.firefox.installedVersion');
				
				if (currentVersion > installedVersion) {
					Services.prefs.setCharPref('extensions.org.gnucitizen.websecurify.firefox.installedVersion', currentVersion);
					
					handleWebsecurifyUpgrade();
				}
			} catch (e) {
				handleWebsecurifyReinstall();
			}
		}
	});
}

addEventListener('load', handleWebsecurifyLoadEvent, false);
