function handleLoadEvent(event) {
	if (event.target != document) {
		return;
	}
	
	let $contentIframe = document.getElementById('about-window-content-iframe');
	
	document.title = $contentIframe.contentDocument.title;
}

addEventListener('load', handleLoadEvent, false);
