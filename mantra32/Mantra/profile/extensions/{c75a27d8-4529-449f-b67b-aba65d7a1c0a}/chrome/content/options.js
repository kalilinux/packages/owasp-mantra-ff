	var WDTTogglerOptions = {

		handleDOMContentLoaded:function(event){
		
			var mainDocument= document;

            var wdttogglerrowtextbox = mainDocument.getElementById("wdttoggler-key-wdttoggler-row").getElementsByTagName("textbox")[0];
			var a = WDTTogglerOptions.getPrefValue("extensions.wdttoggler.customwdttogglerkeyboardshortcut","char");
            if(a) wdttogglerrowtextbox.setAttribute("value", a.toUpperCase());
            else wdttogglerrowtextbox.setAttribute("value", "t".toUpperCase());

            /*var autoscrollrowtextbox = mainDocument.getElementById("wdttoggler-key-autoscroll-row").getElementsByTagName("textbox")[0];
			var b = WDTTogglerOptions.getPrefValue("extensions.wdttoggler.customautoscrollkeyboardshortcut","char");
            if(b) autoscrollrowtextbox.setAttribute("value", b.toUpperCase());
            else autoscrollrowtextbox.setAttribute("value", "a".toUpperCase());*/
			
			var isMac = WDTTogglerOptions.isMac();

			if(isMac) {
		 
				var wdttogglerdescription = mainDocument.getElementById("wdttoggler-key-wdttoggler-description");
				//var autoscrolldescription = mainDocument.getElementById("wdttoggler-key-autoscroll-description");
				wdttogglerdescription.firstChild.nodeValue="Cmd+Option+"
				//autoscrolldescription.firstChild.nodeValue="Cmd+Shift+"
			
			}
			
			else {
			
				var wdttogglerdescription = mainDocument.getElementById("wdttoggler-key-wdttoggler-description");
				//var autoscrolldescription = mainDocument.getElementById("wdttoggler-key-autoscroll-description");
				wdttogglerdescription.firstChild.nodeValue="Ctrl+Alt+"
				//autoscrolldescription.firstChild.nodeValue="Ctrl+Shift+"	
				
			}
			
		},
		
		prefInstance:Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefBranch),
		getPrefValue:function(pref,type){
			if(type="char") return WDTTogglerOptions.prefInstance.getCharPref(pref);
			else if(type="bool") return WDTTogglerOptions.prefInstance.getBoolPref(pref);
			else if(type="int") return WDTTogglerOptions.prefInstance.getIntPref(pref);
		},
		setPrefValue:function(pref,type,value){
			if(type="char") WDTTogglerOptions.prefInstance.setCharPref(pref,value);
			else if(type="bool") WDTTogglerOptions.prefInstance.setBoolPref(pref,value);
			else if(type="int") WDTTogglerOptions.prefInstance.setIntPref(pref,value);
		},
		
		saveKeyboardShortcutChanges:function(event){

			var mainDocument= document;	
			
			var wdttogglerrowtextbox = mainDocument.getElementById("wdttoggler-key-wdttoggler-row").getElementsByTagName("textbox")[0];
			WDTTogglerOptions.setPrefValue("extensions.wdttoggler.customwdttogglerkeyboardshortcut","char",wdttogglerrowtextbox.value.toLowerCase());

            //var autoscrollrowtextbox = mainDocument.getElementById("wdttoggler-key-autoscroll-row").getElementsByTagName("textbox")[0];
			//WDTTogglerOptions.setPrefValue("extensions.wdttoggler.customautoscrollkeyboardshortcut","char",autoscrollrowtextbox.value.toLowerCase());

			WDTTogglerOptions.createAlertPrompt("Please restart your browser to make the changes take effect!")
			
			window.close();

		},
			
		createAlertPrompt:function(promptString){
			
			var prompt = Components.classes["@mozilla.org/embedcomp/prompt-service;1"]
									.getService(Components.interfaces.nsIPromptService);
									
			prompt.alert(null, "Toggle Web Developer Toolbar",promptString);

		
		},
		
		createPromptPrompt:function(promptString){
			
			var prompt = Components.classes["@mozilla.org/embedcomp/prompt-service;1"]
									.getService(Components.interfaces.nsIPromptService);
			
			var input = {value:""}; 
			
			var check = {value:false};

			var result= prompt.prompt(null,"Toggle Web Developer Toolbar",promptString,input,null,check);
			
			if(result==false) return null;
			else return input.value;
			
		},
		
		createConfirmPrompt:function(promptString){
			
			var prompt = Components.classes["@mozilla.org/embedcomp/prompt-service;1"]
									.getService(Components.interfaces.nsIPromptService);			
			
			return prompt.confirm(null, "Toggle Web Developer Toolbar",promptString);

		},
		
		isMac : function ()
		{
			var appInfo = Components.classes["@mozilla.org/xre/app-info;1"];

			if(appInfo)
			{
				if(appInfo.getService(Components.interfaces.nsIXULRuntime).OS == "Darwin" || navigator.platform.indexOf("Mac") == 0)
				{
					return true;
				}
			}

			return false;
			
		},
				
		load:function () {
		    document.getElementById("hidesbicon").addEventListener("command", function (event) {
			    var sss = Components.classes['@mozilla.org/content/style-sheet-service;1'].getService(Components.interfaces.nsIStyleSheetService);
			    var ios = Components.classes['@mozilla.org/network/io-service;1'].getService(Components.interfaces.nsIIOService);
			    var css = "@namespace url(http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul);#wdttoggler-panel {display: none !important;}";
			    var uri = ios.newURI("data:text/css," + encodeURIComponent(css), null, null);
			    if (!sss.sheetRegistered(uri, sss.USER_SHEET)) {
				    sss.loadAndRegisterSheet(uri, sss.USER_SHEET);
				    var wdttogglerprefsinstance = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefBranch);
				    var wdttogglersbiconhide = wdttogglerprefsinstance.getBoolPref("extensions.wdttoggler.sbiconhide");wdttogglerprefsinstance.setBoolPref("extensions.wdttoggler.sbiconhide", true);
			    } else {
				    sss.unregisterSheet(uri, sss.USER_SHEET);
				    var wdttogglerprefsinstance = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefBranch);
				    var wdttogglersbiconhide = wdttogglerprefsinstance.getBoolPref("extensions.wdttoggler.sbiconhide");
				    wdttogglerprefsinstance.setBoolPref("extensions.wdttoggler.sbiconhide", false);
			    }
		    }, false);
		}

	}
	
	window.addEventListener("DOMContentLoaded",WDTTogglerOptions.handleDOMContentLoaded,false);