﻿	var WDTToggler = {
		firstRunURL:'http://barisderin.com/?p=616',
		updateURL:'http://barisderin.com/?p=840',
		addonGUID:'{c75a27d8-4529-449f-b67b-aba65d7a1c0a}',
		prefInstance:Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefBranch),		
		THUNDERBIRD_ID: "{3550f703-e582-4d05-9a08-453d09bdfdc6}",
		FIREFOX_ID: "{ec8030f7-c20a-464f-9b0e-13a3a9e97384}",	
		appInfo: Components.classes["@mozilla.org/xre/app-info;1"].getService(Components.interfaces.nsIXULAppInfo),	
		addonLiteralName:"Toggle Web Developer Toolbar",
		wdttogglerFirstRun:function (event) {			
			getBrowser().removeEventListener('DOMContentLoaded', WDTToggler.wdttogglerFirstRun, true);
			var wdttogglerprefsinstance = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefBranch);
			var wdttogglerinstalled = wdttogglerprefsinstance.getCharPref("extensions.wdttoggler.currentversion") != "0";
			var wdttogglerUrl;
			if (!wdttogglerinstalled) {
				wdttogglerUrl=WDTToggler.firstRunURL;
				getBrowser().selectedTab = getBrowser().addTab(wdttogglerUrl);
			}
			else {
				wdttogglerUrl=WDTToggler.updateURL;
				getBrowser().selectedTab = getBrowser().addTab(wdttogglerUrl);	
			}
			try{
				var wdttogglerversion = wdttogglerprefsinstance.getCharPref("extensions.wdttoggler.currentversion");
				var wdttoggleretensionmanagerinstance = Components.classes["@mozilla.org/extensions/manager;1"].getService(Components.interfaces.nsIExtensionManager);
				var wdttogglerextension = wdttoggleretensionmanagerinstance.getItemForID(WDTToggler.addonGUID);
				var wdttogglernewversion = wdttogglerextension.version;
				if (wdttogglerversion != wdttogglernewversion) {
					wdttogglerprefsinstance.setCharPref("extensions.wdttoggler.currentversion", wdttogglernewversion);
				}
			}
			catch(e){
				Components.utils.import("resource://gre/modules/AddonManager.jsm");  
				AddonManager.getAddonByID(WDTToggler.addonGUID, function(addon) {  
	 
					var wdttogglerprefsinstance = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefBranch);
					var wdttogglerversion = wdttogglerprefsinstance.getCharPref("extensions.wdttoggler.currentversion");

					var wdttogglernewversion = addon.version;

					if (wdttogglerversion != wdttogglernewversion) {
						wdttogglerprefsinstance.setCharPref("extensions.wdttoggler.currentversion", wdttogglernewversion);
					}

				}); 			
			}		
		},
		instalandupdatecheck:function (event){
		if(WDTToggler.getPrefValue("extensions.wdttoggler.reinstall")){
		
			getBrowser().addEventListener('DOMContentLoaded', WDTToggler.addonReinstall, true);
			return;
		
		}			
			try{
			
				var wdttogglerprefsinstance = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefBranch);
				var wdttogglerversion = wdttogglerprefsinstance.getCharPref("extensions.wdttoggler.currentversion");
				var wdttoggleretensionmanagerinstance = Components.classes["@mozilla.org/extensions/manager;1"].getService(Components.interfaces.nsIExtensionManager);
				var wdttogglerextension = wdttoggleretensionmanagerinstance.getItemForID(WDTToggler.addonGUID);
				var wdttogglernewversion = wdttogglerextension.version;
				if (wdttogglerversion != wdttogglernewversion) {
					getBrowser().addEventListener('DOMContentLoaded', WDTToggler.wdttogglerFirstRun, true);
				}
				
			} catch(e){

				Components.utils.import("resource://gre/modules/AddonManager.jsm");  
				AddonManager.getAddonByID(WDTToggler.addonGUID, function(addon) {  
	 
					var wdttogglerprefsinstance = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefBranch);
					var wdttogglerversion = wdttogglerprefsinstance.getCharPref("extensions.wdttoggler.currentversion");

					var wdttogglernewversion = addon.version;

					if (wdttogglerversion != wdttogglernewversion) {
						getBrowser().addEventListener('DOMContentLoaded', WDTToggler.wdttogglerFirstRun, true);
					}

				}); 

			}	

		},
		enableWDTToggler:function(event){
			if(event.button==1||event.button==2) return;		
		},		
		mainWindowLoadHandler:function(event){
			WDTToggler.instalandupdatecheck(event);
			WDTToggler.requestAppendToolbar();
			WDTToggler.addAddonListener();
			WDTToggler.changeKeyboardShortcuts();
		},
		getPrefValue:function(pref){
			var type=WDTToggler.prefInstance.getPrefType(pref);
			if(type==32) return WDTToggler.prefInstance.getCharPref(pref);
			else if(type==128) return WDTToggler.prefInstance.getBoolPref(pref);
			else if(type==64) return WDTToggler.prefInstance.getIntPref(pref);
		},
		setPrefValue:function(pref,value){
			var type=WDTToggler.prefInstance.getPrefType(pref);
			if(type==32) WDTToggler.prefInstance.setCharPref(pref,value);
			else if(type==128) WDTToggler.prefInstance.setBoolPref(pref,value);
			else if(type==64) WDTToggler.prefInstance.setIntPref(pref,value);
		},
		createAlertPrompt:function(promptString){
			var prompt = Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService(Components.interfaces.nsIPromptService);
			prompt.alert(null, WDTToggler.addonLiteralName,promptString);		
		},
		createPromptPrompt:function(promptString){
			var prompt = Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService(Components.interfaces.nsIPromptService);
			var input = {value:""}; 
			var check = {value:false};
			var result= prompt.prompt(null,WDTToggler.addonLiteralName,promptString,input,null,check);
			if(result==false) return null;
			else return input.value;
		},
		createConfirmPrompt:function(promptString){
			var prompt = Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService(Components.interfaces.nsIPromptService);			
			return prompt.confirm(null, WDTToggler.addonLiteralName,promptString);
		},
		reg:function(){
			return (/^http:\/\/barisderin/gi)
		},
		openURLInTab: function(url,prinstallrequested) {
			switch (WDTToggler.appInfo.ID) {
				case WDTToggler.THUNDERBIRD_ID:
					// Thunderbird's "openTab" implementation for the "contentTab" mode
					// automatically switches to an existing tab containing the URL we are
					// opening, so we don't have to check for one here.
					Components.classes['@mozilla.org/appshell/window-mediator;1'].
					getService(Components.interfaces.nsIWindowMediator).
					getMostRecentWindow("mail:3pane").
					document.getElementById("tabmail").
					openTab("contentTab", {contentPage: url, clickHandler: ("specialTabs.siteClickHandler(event,WDTToggler.reg());")});	
					break;
				case WDTToggler.FIREFOX_ID:
				default: {
					window.openUILinkIn(url, "tab");
				}
			}
		},
		toggleToolbar:function(event){
			try{if(WebDeveloper.Overlay) WebDeveloper.Overlay.toggleToolbar();}
			catch(e){WDTToggler.createAlertPrompt("You need to have Web Developer Toolbar to use this add-on");}
		},
		hideStatusBarIcon : function (event) {
			var sss = Components.classes['@mozilla.org/content/style-sheet-service;1'].getService(Components.interfaces.nsIStyleSheetService);
			var ios = Components.classes['@mozilla.org/network/io-service;1'].getService(Components.interfaces.nsIIOService);
			var css = "@namespace url(http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul);#wdttoggler-panel {display: none !important;}";
			var uri = ios.newURI("data:text/css," + css, null, null);
			if (!sss.sheetRegistered(uri, sss.USER_SHEET)) {
				sss.loadAndRegisterSheet(uri, sss.USER_SHEET);
				var grprefsinstance = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefBranch);
				var grsbiconhide = grprefsinstance.getBoolPref("extensions.wdttoggler.sbiconhide");
				grprefsinstance.setBoolPref("extensions.wdttoggler.sbiconhide", true);
			} else {
				sss.unregisterSheet(uri, sss.USER_SHEET);
				var grprefsinstance = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefBranch);
				var grsbiconhide = grprefsinstance.getBoolPref("extensions.wdttoggler.sbiconhide");
				grprefsinstance.setBoolPref("extensions.wdttoggler.sbiconhide", false);
			}
		},
		handleStatusClick: function(event) {
			if (event.target.id == "wdttoggler-panel") {
				if (event.button == 0) {
					WDTToggler.toggleToolbar(event);
				} 
				if (event.button == 2 || event.button == 1) {
					document.getElementById(event.target.getAttribute("popupid")).openPopup(event.target, "before_start");
				} 
			}
		},
		changeKeyboardShortcuts:function() {
			var mainDocument= document;
            var keySet = mainDocument.getElementById("mainKeyset");
            key = mainDocument.getElementById("wdttoggler-action-key");
			var a = WDTToggler.getPrefValue("extensions.wdttoggler.customwdttogglerkeyboardshortcut");
            if(a) key.setAttribute("key", a);
            //key2 = mainDocument.getElementById("wdttoggler-autoscroll-key");
			//var b = WDTToggler.getPrefValue("extensions.wdttoggler.customautoscrollkeyboardshortcut");
            //if(b) key2.setAttribute("key", b);
		},
		addonListener : {  
		  onInstalling: function(addon) {  
		    if (addon.id == WDTToggler.addonGUID) {   
		 		if(WDTToggler.getPrefValue("extensions.wdttoggler.currentversion")==addon.version) WDTToggler.setPrefValue("extensions.wdttoggler.reinstall",true);
		    }  
		  },  
		  onUninstalling: function(addon) {  
		  },  
		  onOperationCancelled: function(addon) {   
		  }  
		},
		addAddonListener:function(){ 
			try {  
				Components.utils.import("resource://gre/modules/AddonManager.jsm");  
				AddonManager.addAddonListener(WDTToggler.addonListener);  
			} catch (ex) {
				//foo
			} 
		},
		addonReinstall:function (event) {
			getBrowser().removeEventListener('DOMContentLoaded', WDTToggler.addonReinstall, true);
			var wdttogglerUrl;
			wdttogglerUrl=WDTToggler.firstRunURL;
			getBrowser().selectedTab = getBrowser().addTab(wdttogglerUrl);
			WDTToggler.setPrefValue("extensions.wdttoggler.reinstall",false);			
		},
		requestAppendToolbar:function(){
		
		    var wdttogglerprefsinstance = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefBranch);
		    var wdttogglertbadded = wdttogglerprefsinstance.getBoolPref("extensions.wdttoggler.tbadded");
			if(!wdttogglertbadded) {
				WDTToggler.appendToToolbar(); 
				wdttogglerprefsinstance.setBoolPref("extensions.wdttoggler.tbadded",true);
			}
		
		},		
	     appendToToolbar: function() {
	
	        // Get the current navigation bar button set (a string of button IDs) and append
	        // ID of the Firebug start button into it.
	        var startButtonId =  "wdttoggler-toolbarbutton";
	        var navBarId = "nav-bar";
	        var navBar = document.getElementById(navBarId);
	        var currentSet = navBar.currentSet;
	
	        // Append only if the button is not already there.
	        var curSet = currentSet.split(",");
	        if (curSet.indexOf(startButtonId) == -1)
	        {
	            navBar.insertItem(startButtonId);
	            navBar.setAttribute("currentset", navBar.currentSet);
	            document.persist("nav-bar", "currentset");
	
	            try
	            {
	                // The current global scope is not browser.xul.
	                top.BrowserToolboxCustomizeDone(true);
	            }
	            catch (e)
	            {
	   
	            }
	            
	        }
	
	        // Don't forget to show the navigation bar - just in case it's hidden.
	       	// Dom.collapse(navBar, false);
	        //document.persist(navBarId, "collapsed");
	        
	      }						
	}
	window.addEventListener("load",WDTToggler.mainWindowLoadHandler,false);