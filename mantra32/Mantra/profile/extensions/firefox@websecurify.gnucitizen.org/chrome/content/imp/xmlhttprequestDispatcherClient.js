// HELPERS:
// addMessageListener - for receiving messages from xmlhttprequestDispatcherChrome.js
// sendAsyncMessage - for sending messages to xmlhttprequestDispatcherChrome.js
// addEventListener - to register for global DOM events 
// websecurifyXmlhttprequestDispatcherClientHelpers.isDocumentInScope - to check if a document is in scope for this content script
// websecurifyXmlhttprequestDispatcherClientHelpers.getDocument - to get the document
// websecurifyXmlhttprequestDispatcherClientHelpers.createEvent - to create an event from the document object
// websecurifyXmlhttprequestDispatcherClientHelpers.getElementById - to get an element from the document object
// websecurifyXmlhttprequestDispatcherClientHelpers.getElementTextData - to get the text data of an element
// websecurifyXmlhttprequestDispatcherClientHelpers.setElementTextData - to set the text data of an element

/* ------------------------------------------------------------------------ */

(function () {
	var helpers = websecurifyXmlhttprequestDispatcherClientHelpers;
	
	/* -------------------------------------------------------------------- */
	
	function sendEventToDispatcher(eventName, data) {
		var event = helpers.createEvent('Event');
		
		event.initEvent(eventName, true, true);
		
		var $dispatcher = helpers.getElementById('websecurify-xmlhttprequest-dispatcher');
		
		helpers.setElementTextData($dispatcher, JSON.stringify(data));
		
		$dispatcher.dispatchEvent(event);
	}
	
	/* -------------------------------------------------------------------- */
	
	function handleXMLHttpRequest_onreadystatechange(event) {
		sendEventToDispatcher('XMLHttpRequest_onreadystatechange', event.json);
	}
	
	function handleXMLHttpRequest_onload(event) {
		sendEventToDispatcher('XMLHttpRequest_onload', event.json);
	}
	
	function handleXMLHttpRequest_onerror(event) {
		sendEventToDispatcher('XMLHttpRequest_onerror', event.json);
	}
	
	function handleXMLHttpRequest_onabort(event) {
		sendEventToDispatcher('XMLHttpRequest_onabort', event.json);
	}
	
	addMessageListener('websecurify-xmlhttprequest-onreadystatechange', handleXMLHttpRequest_onreadystatechange);
	addMessageListener('websecurify-xmlhttprequest-onload', handleXMLHttpRequest_onload);
	addMessageListener('websecurify-xmlhttprequest-onerror', handleXMLHttpRequest_onerror);
	addMessageListener('websecurify-xmlhttprequest-onabort', handleXMLHttpRequest_onabort);
	
	/* -------------------------------------------------------------------- */
	
	function handleXMLHttpRequestEvent(event) {
		sendAsyncMessage('websecurify-xmlhttprequest', {json: helpers.getElementTextData(event.target)});
	}
	
	/* -------------------------------------------------------------------- */
	
	function augmentPage() {
		helpers.getDocument().body.setAttribute('websecurifyExtensionSupport', 'firefox');
		
		var xmlhttprequestDispatcher = helpers.getElementById('websecurify-xmlhttprequest-dispatcher');
		
		if (xmlhttprequestDispatcher) {
			xmlhttprequestDispatcher.addEventListener('XMLHttpRequest', handleXMLHttpRequestEvent, false);
		} else {
			helpers.getDocument().addEventListener('DOMNodeInserted', function (event) {
				if (event.target.id == 'websecurify-xmlhttprequest-dispatcher') {
					helpers.getDocument().removeEventListener('DOMNodeInserted', arguments.callee, false);
					event.target.addEventListener('XMLHttpRequest', handleXMLHttpRequestEvent, false);
				}
			}, false);
		}
	}
	
	/* -------------------------------------------------------------------- */
	
	function handleDOMContentLoadedEvent(event) {
		if (event.target != helpers.getDocument()) {
			return;
		}
		
		if (!helpers.isDocumentInScope(helpers.getDocument())) {
			return;
		}
		
		augmentPage();
	}
	
	addEventListener('DOMContentLoaded', handleDOMContentLoadedEvent, false);
})();
