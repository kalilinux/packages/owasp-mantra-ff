	var WDTToggler = {
		firstRunURL:'http://barisderin.com/?p=616',
		updateURL:'http://barisderin.com/?p=840',
		addonGUID:'{c75a27d8-4529-449f-b67b-aba65d7a1c0a}',
		prefInstance:Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefBranch),		
		THUNDERBIRD_ID: "{3550f703-e582-4d05-9a08-453d09bdfdc6}",
		FIREFOX_ID: "{ec8030f7-c20a-464f-9b0e-13a3a9e97384}",	
		appInfo: Components.classes["@mozilla.org/xre/app-info;1"].getService(Components.interfaces.nsIXULAppInfo),	
		addonLiteralName:"Toggle Web Developer Toolbar",
		wdttogglerFirstRun:function (event) {			
			getBrowser().removeEventListener('DOMContentLoaded', WDTToggler.wdttogglerFirstRun, true);
			var wdttogglerprefsinstance = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefBranch);
			var wdttogglerinstalled = wdttogglerprefsinstance.getCharPref("extensions.wdttoggler.currentversion") != "0";
			var wdttogglerUrl;
			if (!wdttogglerinstalled) {
				wdttogglerUrl=WDTToggler.firstRunURL;
				getBrowser().selectedTab = getBrowser().addTab(wdttogglerUrl);
			}
			else {
				wdttogglerUrl=WDTToggler.updateURL;
				getBrowser().selectedTab = getBrowser().addTab(wdttogglerUrl);	
			}
			AddonManager.getAddonByID(WDTToggler.addonGUID, function(addon) {  
 
				var wdttogglerprefsinstance = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefBranch);
				var wdttogglerversion = wdttogglerprefsinstance.getCharPref("extensions.wdttoggler.currentversion");

				var wdttogglernewversion = addon.version;

				if (wdttogglerversion != wdttogglernewversion) {
					wdttogglerprefsinstance.setCharPref("extensions.wdttoggler.currentversion", wdttogglernewversion);
				}

			});	
		},
		instalandupdatecheck:function (event){
			if(WDTToggler.getPrefValue("extensions.wdttoggler.reinstall")){
			
				getBrowser().addEventListener('DOMContentLoaded', WDTToggler.addonReinstall, true);
				return;
			
			}			 
			AddonManager.getAddonByID(WDTToggler.addonGUID, function(addon) {  
 
				var wdttogglerprefsinstance = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefBranch);
				var wdttogglerversion = wdttogglerprefsinstance.getCharPref("extensions.wdttoggler.currentversion");

				var wdttogglernewversion = addon.version;

				if (wdttogglerversion != wdttogglernewversion) {
					getBrowser().addEventListener('DOMContentLoaded', WDTToggler.wdttogglerFirstRun, true);
				}

			}); 	
		},
		enableWDTToggler:function(event){
			if(event.button==1||event.button==2) return;		
		},		
		getPrefValue:function(pref){
			var type=WDTToggler.prefInstance.getPrefType(pref);
			if(type==32) return WDTToggler.prefInstance.getCharPref(pref);
			else if(type==128) return WDTToggler.prefInstance.getBoolPref(pref);
			else if(type==64) return WDTToggler.prefInstance.getIntPref(pref);
		},
		setPrefValue:function(pref,value){
			var type=WDTToggler.prefInstance.getPrefType(pref);
			if(type==32) WDTToggler.prefInstance.setCharPref(pref,value);
			else if(type==128) WDTToggler.prefInstance.setBoolPref(pref,value);
			else if(type==64) WDTToggler.prefInstance.setIntPref(pref,value);
		},
		createAlertPrompt:function(promptString){
			var prompt = Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService(Components.interfaces.nsIPromptService);
			prompt.alert(null, WDTToggler.addonLiteralName,promptString);		
		},
		createPromptPrompt:function(promptString){
			var prompt = Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService(Components.interfaces.nsIPromptService);
			var input = {value:""}; 
			var check = {value:false};
			var result= prompt.prompt(null,WDTToggler.addonLiteralName,promptString,input,null,check);
			if(result==false) return null;
			else return input.value;
		},
		createConfirmPrompt:function(promptString){
			var prompt = Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService(Components.interfaces.nsIPromptService);			
			return prompt.confirm(null, WDTToggler.addonLiteralName,promptString);
		},
		reg:function(){
			return (/^http:\/\/barisderin/gi)
		},
		openURLInTab: function(url,prinstallrequested) {
			switch (WDTToggler.appInfo.ID) {
				case WDTToggler.THUNDERBIRD_ID:
					// Thunderbird's "openTab" implementation for the "contentTab" mode
					// automatically switches to an existing tab containing the URL we are
					// opening, so we don't have to check for one here.
					Components.classes['@mozilla.org/appshell/window-mediator;1'].
					getService(Components.interfaces.nsIWindowMediator).
					getMostRecentWindow("mail:3pane").
					document.getElementById("tabmail").
					openTab("contentTab", {contentPage: url, clickHandler: ("specialTabs.siteClickHandler(event,WDTToggler.reg());")});	
					break;
				case WDTToggler.FIREFOX_ID:
				default: {
					window.openUILinkIn(url, "tab");
				}
			}
		},
		toggleToolbar:function(event){
			if(document.getElementById("webdeveloper-toolbar")) webdeveloper_toggleToolbar();
			else WDTToggler.createAlertPrompt("You need to have Web Developer Toolbar to use this add-on");
		},
		hideStatusBarIcon : function (event) {
			var sss = Components.classes['@mozilla.org/content/style-sheet-service;1'].getService(Components.interfaces.nsIStyleSheetService);
			var ios = Components.classes['@mozilla.org/network/io-service;1'].getService(Components.interfaces.nsIIOService);
			var css = "@namespace url(http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul);#wdttoggler-panel {display: none !important;}";
			var uri = ios.newURI("data:text/css," + css, null, null);
			if (!sss.sheetRegistered(uri, sss.USER_SHEET)) {
				sss.loadAndRegisterSheet(uri, sss.USER_SHEET);
				var grprefsinstance = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefBranch);
				var grsbiconhide = grprefsinstance.getBoolPref("extensions.wdttoggler.sbiconhide");
				grprefsinstance.setBoolPref("extensions.wdttoggler.sbiconhide", true);
			} else {
				sss.unregisterSheet(uri, sss.USER_SHEET);
				var grprefsinstance = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefBranch);
				var grsbiconhide = grprefsinstance.getBoolPref("extensions.wdttoggler.sbiconhide");
				grprefsinstance.setBoolPref("extensions.wdttoggler.sbiconhide", false);
			}
		},
		handleStatusClick: function(event) {
			if (event.target.id == "wdttoggler-panel") {
				if (event.button == 0) {
					WDTToggler.toggleToolbar(event);
				} 
				if (event.button == 2 || event.button == 1) {
					document.getElementById(event.target.getAttribute("popupid")).openPopup(event.target, "before_start");
				} 
			}
		},
		handleStatusClick2: function(event) {
			WDTToggler.toggleToolbar(event);
		},		
		changeKeyboardShortcuts:function() {
			var mainDocument= document;
            var keySet = mainDocument.getElementById("mainKeyset");
            key = mainDocument.getElementById("wdttoggler-action-key");
			var a = WDTToggler.getPrefValue("extensions.wdttoggler.customwdttogglerkeyboardshortcut");
            if(a) key.setAttribute("key", a);
            //key2 = mainDocument.getElementById("wdttoggler-autoscroll-key");
			//var b = WDTToggler.getPrefValue("extensions.wdttoggler.customautoscrollkeyboardshortcut");
            //if(b) key2.setAttribute("key", b);
		},
		addAddonListener:function(){

var beingUninstalled;  
  
let listener = {  
  onInstalling: function(addon) {  
    if (addon.id == WDTToggler.addonGUID) {  
      beingUninstalled = true;  

 if(WDTToggler.getPrefValue("extensions.wdttoggler.currentversion")==addon.version) 			      WDTToggler.setPrefValue("extensions.wdttoggler.reinstall",true);
 
    }  
  },  
  onUninstalling: function(addon) {  
  },  
  onOperationCancelled: function(addon) { 
  }  
}  
  
try {  
  Components.utils.import("resource://gre/modules/AddonManager.jsm");  
  AddonManager.addAddonListener(listener);  
} catch (ex) {} 
		
		},
		
		addonReinstall:function (event) {
			getBrowser().removeEventListener('DOMContentLoaded', WDTToggler.addonReinstall, true);
			var wdttogglerUrl;
			wdttogglerUrl=WDTToggler.firstRunURL;
			getBrowser().selectedTab = getBrowser().addTab(wdttogglerUrl);
			WDTToggler.setPrefValue("extensions.wdttoggler.reinstall",false);			
		},
		
		prefInstance:Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefBranch),
		getPrefValue:function(pref){
			var type=WDTToggler.prefInstance.getPrefType(pref);
			if(type==32) return WDTToggler.prefInstance.getCharPref(pref);
			else if(type==128) return WDTToggler.prefInstance.getBoolPref(pref);
			else if(type==64) return WDTToggler.prefInstance.getIntPref(pref);
		},
		setPrefValue:function(pref,value){
			var type=WDTToggler.prefInstance.getPrefType(pref);
			if(type==32) WDTToggler.prefInstance.setCharPref(pref,value);
			else if(type==128) WDTToggler.prefInstance.setBoolPref(pref,value);
			else if(type==64) WDTToggler.prefInstance.setIntPref(pref,value);
		},
		
		mainWindowLoadHandler:function(event){
			window.removeEventListener("DOMContentLoaded",WDTToggler.mainWindowLoadHandler,false);
			//WDTToggler.addStatusbarButton();
			WDTToggler.addToolbarButton();
			WDTToggler.instalandupdatecheck(event);		
			WDTToggler.addAddonListener();
			WDTToggler.addKey();
			WDTToggler.changeKeyboardShortcuts();
		},			
		init:function(){
			window.addEventListener("DOMContentLoaded",WDTToggler.mainWindowLoadHandler,false);
		},
		init2:function(){
			WDTToggler.mainWindowLoadHandler(); 
		},
		addStatusbarButton:function(){
			var wdttogglerTBB = document.createElement("statusbarpanel");
			wdttogglerTBB.setAttribute("id", "wdttoggler-panel");
			wdttogglerTBB.setAttribute("style", "cursor:pointer;"); 
			var wdttogglerTBB2 = document.createElement("image");
			wdttogglerTBB2.setAttribute("src", "resource://wdttoggler/icon16.png");
			wdttogglerTBB2.setAttribute("context", "");			
			wdttogglerTBB.appendChild(wdttogglerTBB2);
			document.getElementById("status-bar").appendChild(wdttogglerTBB);
			wdttogglerTBB.addEventListener("click",WDTToggler.handleStatusClick2, true);		
		},
		removeStatusbarButton:function(){
			document.getElementById("wdttoggler-panel").parentNode.removeChild(document.getElementById("wdttoggler-panel"));		
		},
		mainWindowUnloadHandler:function(event){
			//WDTToggler.removeStatusbarButton();
			WDTToggler.removeToolbarButton();
			WDTToggler.removeKey();
			window.WDTToggler=null;
			WDTToggler=null;
		},
		addKey:function(){
			  let wdttogglerKeyset = document.createElement("keyset");
			  wdttogglerKeyset.setAttribute("id", "wdttoggler-action-keyset");
			
			  // add hotkey
			  	var wdttogglerKey = document.createElement("key");
			    wdttogglerKey.setAttribute("id", "wdttoggler-action-key");
			    var a = WDTToggler.getPrefValue("extensions.wdttoggler.customwdttogglerkeyboardshortcut");
			    wdttogglerKey.setAttribute("key", (a=="") ? "t" : a);
			    wdttogglerKey.setAttribute("modifiers", "accel alt");
			    wdttogglerKey.setAttribute("oncommand", "WDTToggler.toggleToolbar(event);");
			    //wdttogglerKey.addEventListener("command", function(event){WDTToggler.toggleToolbar(event);}, true);
			    document.getElementById("main-window").appendChild(wdttogglerKeyset).appendChild(wdttogglerKey);

		},
		removeKey:function(){
			document.getElementById("wdttoggler-action-keyset").parentNode.removeChild(document.getElementById("wdttoggler-action-keyset"));
		},
		addToolbarButton:function(){
			
			var wdttoggler_toolbarbutton = document.createElement("toolbarbutton");
			wdttoggler_toolbarbutton.setAttribute("id", "wdttoggler-toolbarbutton");
			wdttoggler_toolbarbutton.setAttribute("style", "list-style-image:url('resource://wdttoggler/icon16.png')");			
			wdttoggler_toolbarbutton.setAttribute("label", "Toggle Web Developer Toolbar"); 
			wdttoggler_toolbarbutton.setAttribute("tooltiptext", "Click to Toggle Web Developer Toolbar");
			wdttoggler_toolbarbutton.setAttribute("class", "toolbarbutton-1");
			wdttoggler_toolbarbutton.setAttribute("type", "button");
			wdttoggler_toolbarbutton.addEventListener("command", function(event){WDTToggler.toggleToolbar(event);},true);	

			document.getElementById("nav-bar").appendChild(wdttoggler_toolbarbutton);

				
		},
		removeToolbarButton:function(){
			document.getElementById("wdttoggler-toolbarbutton").parentNode.removeChild(document.getElementById("wdttoggler-toolbarbutton"));		
		}											
	}
	//window.addEventListener("load",WDTToggler.mainWindowLoadHandler,false);
	window.wrappedJSObject.WDTToggler=WDTToggler;