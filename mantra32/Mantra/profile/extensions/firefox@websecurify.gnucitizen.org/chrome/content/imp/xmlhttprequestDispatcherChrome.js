(function () {
	function createWebsecurifyReadystatechangeHandler(xmlHttpRequest, options, messageManager) {
		return function () {
			var readyState = xmlHttpRequest.readyState;
			
			var data = {
				id: options.id,
				readyState: readyState
			};
			
			if (readyState >= 2) {
				data.status = xmlHttpRequest.status;
				data.statusText = xmlHttpRequest.statusText;
				data.allResponseHeaders = xmlHttpRequest.getAllResponseHeaders();
			}
			
			if (readyState >= 3) {
				data.responseText = xmlHttpRequest.responseText;
			}
			
			messageManager.sendAsyncMessage('websecurify-xmlhttprequest-onreadystatechange', data);
		};
	}
	
	function createWebsecurifyXMLHttpRequestLoadHandler(xmlHttpRequest, options, messageManager) {
		return function () {
			var readyState = xmlHttpRequest.readyState;
			
			var data = {
				id: options.id,
				readyState: readyState
			};
			
			if (readyState >= 2) {
				data.status = xmlHttpRequest.status;
				data.statusText = xmlHttpRequest.statusText;
				data.allResponseHeaders = xmlHttpRequest.getAllResponseHeaders();
			}
			
			if (readyState >= 3) {
				data.responseText = xmlHttpRequest.responseText;
			}
			
			messageManager.sendAsyncMessage('websecurify-xmlhttprequest-onload', data);
		};
	}
	
	function createWebsecurifyXMLHttpRequestErrorHandler(options, messageManager) {
		return function () {
			var data = {
				id: options.id
			};
			
			messageManager.sendAsyncMessage('websecurify-xmlhttprequest-onerror', data);
		};
	}
	
	function createWebsecurifyXMLHttpRequestAbortHandler(options, messageManager) {
		return function () {
			var data = {
				id: options.id
			};
			
			messageManager.sendAsyncMessage('websecurify-xmlhttprequest-onabort', data);
		};
	}
	
	/* -------------------------------------------------------------------- */
	
	function handleWebsecurifyXmlhttprequest(event) {
		var options = JSON.parse(event.json.json);
		
		if (!/https?:\/\//i.test(options.url)) {
			// TODO: handle errors
			return;
			//
		}
		
		var xmlHttpRequest = new XMLHttpRequest();
		
		// TODO: handle errors
		xmlHttpRequest.open(options.method, options.url, true);
		//
		
		if ('withCredentials' in options && options.withCredentials != null) {
			try {
				xmlHttpRequest.withCredentials = options.withCredentials;
			} catch (e) {
				Components.utils.reportError(e);
				Components.utils.reportError('cannot set withCredentials parameter with value ' + options.withCredentials + ' on XMLHttpRequest object');
			}
		}
		
		if ('overrideMimeType' in options && options.overrideMimeType != null) {
			try {
				xmlHttpRequest.overrideMimeType(options.overrideMimeType);
			} catch (e) {
				Components.utils.reportError(e);
				Components.utils.reportError('cannot call overrideMimeType function with parameter ' + options.overrideMimeType + ' on XMLHttpRequest object');
			}
		}
		
		if ('headers' in options && options.headers != null) {
			var headers = options.headers;
			
			for (var headerName in headers) {
				var headerValue = headers[headerName];
				
				try {
					xmlHttpRequest.setRequestHeader(headerName, headerValue);
				} catch (e) {
					Components.utils.reportError(e);
					Components.utils.reportError('cannot set request header ' + headerName + ' with value ' + headerValue + ' on XMLHttpRequest object');
				}
			}
		}
		
		xmlHttpRequest.onreadystatechange = createWebsecurifyReadystatechangeHandler(xmlHttpRequest, options, event.target.messageManager);
		xmlHttpRequest.onload = createWebsecurifyXMLHttpRequestLoadHandler(xmlHttpRequest, options, event.target.messageManager);
		xmlHttpRequest.onerror = createWebsecurifyXMLHttpRequestErrorHandler(options, event.target.messageManager);
		xmlHttpRequest.onabort = createWebsecurifyXMLHttpRequestAbortHandler(options, event.target.messageManager);
		
		// TODO: handle errors
		xmlHttpRequest.send(options.data);
		//
	}
	
	/* -------------------------------------------------------------------- */
	
	function handleWebsecurifyXmlhttprequestGetDomainWhitelist(event) {
		var domainWhitelist = {};
		var prefix = 'org.gnucitizen.websecurify.firefox.xmlhttprequest.whitelist.';
		
		Services.prefs.getChildList(prefix).forEach(function (key) {
			var suffix = key.substring(prefix.length, key.length);
			var tokens = suffix.split('.');
			
			if (tokens.length != 2) {
				Components.utils.reportError(new Error('unrecognized structure for key ' + key));
				
				return;
			}
			
			var index = tokens[0];
			
			try {
				index = parseInt(index, 10);
			} catch (e) {
				Components.utils.reportError(new Error('unrecognized index for key ' + key));
				
				return;
			}
			
			var type = tokens[1];
			
			if (!(type in {'domain': 1, 'sslonly': 1})) {
				Components.utils.reportError(new Error('unrecognized type for key ' + key));
				
				return;
			}
			
			if (!(index in domainWhitelist)) {
				domainWhitelist[index] = {};
			}
			
			if (type == 'domain') {
				domainWhitelist[index][type] = Services.prefs.getCharPref(key).trim();
			} else
			if (type == 'sslonly') {
				domainWhitelist[index][type] = Services.prefs.getBoolPref(key);
			}
		});
		
		for (var index in domainWhitelist) {
			var entry = domainWhitelist[index];
			
			if (!('domain' in entry)) {
				Components.utils.reportError(new Error('no domain defined for key ' + prefix + index));
				
				delete domainWhitelist[index];
			}
		}
		
		event.target.messageManager.sendAsyncMessage('websecurify-xmlhttprequest-set-domain-whitelist', {list: domainWhitelist});
	}
	
	/* -------------------------------------------------------------------- */
	
	function handleWebsecurifyXmlhttprequestLoadEvent(event) {
		if (event.target != document) {
			return;
		}
		
		messageManager.loadFrameScript('chrome://org.gnucitizen.websecurify.firefox/content/imp/xmlhttprequestDispatcherClientHelpers.js', true);
		messageManager.loadFrameScript('chrome://org.gnucitizen.websecurify.firefox/content/imp/xmlhttprequestDispatcherClient.js', true);
		messageManager.addMessageListener('websecurify-xmlhttprequest', handleWebsecurifyXmlhttprequest);
		messageManager.addMessageListener('websecurify-xmlhttprequest-get-domain-whitelist', handleWebsecurifyXmlhttprequestGetDomainWhitelist);
	}
	
	addEventListener('load', handleWebsecurifyXmlhttprequestLoadEvent, false);
})();
